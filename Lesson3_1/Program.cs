﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace Lesson3_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var filename = Directory.GetCurrentDirectory() + "\\accounts.json";
            var accountMass = new Account[]
            {
                new Account{FirstName="Антон", LastName="Валов", BirthDate= new DateTime(1990,12,1)},
                new Account{FirstName="Pavel", LastName="Milov", BirthDate= new DateTime(1989,05,10)},
                new Account{FirstName="Vasya", LastName="Shpit", BirthDate= new DateTime(1985,09,16)},
                new Account{FirstName="Aleksey", LastName="Kuznetcov", BirthDate= new DateTime(1995,04,20)}
            };
            var repos = new Repos(accountMass, filename);
            repos.AddAccount(new Account() { FirstName = "Artem", LastName = "Vyatkin", BirthDate = new DateTime(2008, 06, 17)});
            var account = new Account() { FirstName = "Artem", LastName = "Vyatkin", BirthDate = new DateTime(1980, 06, 17) };
            Console.WriteLine(repos.SearchAccount(account));
            Console.ReadKey();
        }
    }
}
