﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Lesson3_1
{
    public class DataSerialize : IDataSerialize
    {
        private readonly string filename;
        public DataSerialize(string filename)
        {
            this.filename = filename;
        }
        

        public IEnumerable<Account> DataDeserializer()
        {
            string jsonString = File.ReadAllText(filename);
            Account[] deserializeData = JsonConvert.DeserializeObject<Account[]>(jsonString);
            return deserializeData;
        }

        public void DataSerializer(IEnumerable<Account> accounts)
        {
            var serializeData = JsonConvert.SerializeObject(accounts);

            if (!File.Exists(filename))
                File.Create(filename).Close();
            File.WriteAllText(filename, serializeData);
        }
    }
}
