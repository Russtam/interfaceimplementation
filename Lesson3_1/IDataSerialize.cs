﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson3_1
{
    public interface IDataSerialize
    {
        void DataSerializer(IEnumerable<Account> accounts);
        IEnumerable<Account> DataDeserializer(); 
    }
}
