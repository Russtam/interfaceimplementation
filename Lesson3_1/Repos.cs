﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Lesson3_1
{
    public class Repos : IRepository<Account>, IAccountService
    {
        private List<Account> _accounts;
        private IDataSerialize _serializer;

        public Repos(Account[] accounts, string filename)
        {
            _accounts = new List<Account>();
            if (accounts.Length != 0)
                _accounts.AddRange(accounts);
            _serializer = new DataSerialize(filename);

        }
        public bool AccountIsExist(Account account)
        {
            if (_accounts.Any(acc => acc.FirstName == account.FirstName & acc.LastName == account.LastName))
                return true;
            return false;
        }
        public void Add(Account item)
        {
            _accounts.Add(item);
            _serializer.DataSerializer(_accounts);
        }

        private void LoadData()
        {
            if (_accounts.Count == 0)
            {
                _accounts.AddRange(_serializer.DataDeserializer());
            }
        }

        public IEnumerable<Account> GetAll()
        {
            LoadData();
            return _accounts;
        }

        // Тут я не смог придумать по другому, кроме как реализвать его подобным образом. Просьба дать комментарий по региону Get_One
        #region Get_One
        private Account _account;
        public Account SearchAccount(Account account)
        {
            _account = account;
            return GetOne(AccountIsExist);
        }
        public Account GetOne(Func<Account, bool> predicate)
        {
            LoadData();
            if (predicate.Invoke(_account))
            {
                return _account;
            }
            return null;
        }
        #endregion
        // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
        // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
        public void AddAccount(Account account)
        {
            string pattern = @"[a-zA-ZА-Яа-я]";
            Regex regex = new Regex(pattern);
            if (regex.IsMatch(account.FirstName))
            {
                if (regex.IsMatch(account.LastName))
                {
                    if (IsCheckAge(account))
                    {
                        Add(account);
                    }
                    else
                        Console.WriteLine("Возраст не удовлетворяет возрастным ограничениям");
                }
                else
                    Console.WriteLine("Фамилия должна содержать только буквы русского или латинского алфавита и не должна быть пустым");
            }
            else
                Console.WriteLine("Имя должно содержать только буквы русского или латинского алфавита и не должно быть пустым");
        }

        private bool IsCheckAge(Account account)
        {
            var age = DateTime.Now - account.BirthDate;
            var years = new DateTime(age.Ticks);
            if (years.Year > 18)
                return true;
            return false;
        }
    }
}
